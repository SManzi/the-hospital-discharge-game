/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospitalgamev3;


import com.opencsv.CSVWriter;
import java.awt.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import org.apache.commons.io.FileUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;



/**
 *
 * @author ssm209
 */


public class HospitalGameV3 {
    int turnCount = 1;
    int patients = 12;
    int beds;
    int penaltiesInc = 0;
    int admisPenalty = 100;
    int newPatients = 0;
    int newAdmis = 0;
    int maxTurns = 10;
    int DisNum = 0;
    int DisCount = 0;
    Object[][] patNum;
    
    int patients2 = 15;
    int beds2 = 15;
    int penaltiesInc2 = 0;
    int admisPenalty2 = 100;
    int newPatients2 = 0;
    int DisCount2 = 0;
    Object[][] patNum2;
    int numReadmit = 0;
    int readmitTurn = 0;
    int readmitPenalty = 100;
    int numPatCreated = 0;
    
    int wellnessInc = 10;
    int wellnessInc2 = 5;
    int totDis = 0;
    int totDis2 = 0;
    int totRead = 0;
    int totAvPen = 0;
    int totAvPen2 = 0;
    int totRePen = 0;
    
    int [] meanCalc;
    int [] meanCalc2;
    double[][] meanData = new double[maxTurns*2][3];
    double[][] meanData2 = new double[maxTurns][3];
    double mean = 0;
    int player = 1;
    int counter = 0;
    int counter2 = 0;
    int penaltiesInc3 = 0;
    ArrayList<Integer> meanC;
   // int readCounter = 0;
    //ArrayList<Integer> patDataOut;
    int[][] patDataOut;
    int[][] summaryDataOut;
    int[][] readDataOut;
    int randNewPat = 7;
    int randPatWell = 80;
    JFileChooser chooser;
    String chooserTitle = "Output files save location";
    String saveLocation = "U:\\GameTheory";
    Font buttonFont;
    Font scoreFont;
    Color back = new Color(221, 234, 237);
   // DefaultCategoryDataset chartData = new DefaultCategoryDataset();
    
    JFrame startFrame = new JFrame("Game theory in healthcare");
    JPanel acuteSettings = new JPanel(new GridBagLayout());
    JPanel startControls = new JPanel(new GridLayout(1,2));
    JButton start = new JButton("Set output save location and start Game");
    JButton setSave = new JButton("Set save folder");
    
    JLabel acuteSetName = new JLabel("Acute Hospital Settings", JLabel.CENTER);
    JLabel commSetName = new JLabel("Community Hospital Settings", JLabel.CENTER);
    JLabel otherSetName = new JLabel("Other Settings", JLabel.CENTER);
    
    JLabel gameName = new JLabel("Game theory in healthcare", JLabel.CENTER);
    
    JLabel setBeds = new JLabel("Set number of beds", JLabel.CENTER);
    JTextField setNumBeds = new JTextField("10", 20);
    
    JLabel setPatients = new JLabel("Set number of patients", JLabel.CENTER);
    JTextField setNumPatients = new JTextField("12", 20);
    
    JLabel setWell = new JLabel("Set wellness increase", JLabel.CENTER);
    JTextField setWellInc = new JTextField("10", 20);
    
    JLabel setAdPen = new JLabel("Set penalty for lack of beds", JLabel.CENTER);
    JTextField setValAdPen = new JTextField("100", 20);
    
    JLabel setReadPen = new JLabel("Set penalty for readmission", JLabel.CENTER);
    JTextField setValReadPen = new JTextField("100", 20);
    
    JLabel setBeds2 = new JLabel("Set number of beds", JLabel.CENTER);
    JTextField setNumBeds2 = new JTextField("15", 20);
    
    JLabel setPatients2 = new JLabel("Set number of patients", JLabel.CENTER);
    JTextField setNumPatients2 = new JTextField("15", 20);
    
    JLabel setWell2 = new JLabel("Set wellness increase", JLabel.CENTER);
    JTextField setWellInc2 = new JTextField("5", 20);
    
    JLabel setAdPen2 = new JLabel("Set penalty for lack of beds", JLabel.CENTER);
    JTextField setValAdPen2 = new JTextField("100", 20);
    
    JLabel setRandNewPat = new JLabel("Set upper limit of new patient generation", JLabel.CENTER);
    JTextField setValRandNewPat = new JTextField("7", 20);
    
    JLabel setRandPatWell = new JLabel("Set upper limit of new patient wellness", JLabel.CENTER);
    JTextField setValRandPatWell = new JTextField("80", 20);
    
    JLabel setMaxTurn = new JLabel("Set maximum number of turns", JLabel.CENTER);
    JTextField setValMaxTurn = new JTextField("10", 20);
    
    JFrame frame = new JFrame("Acute Hospital");
    DefaultTableModel model = new DefaultTableModel(new Object[][] {},
            new String[] {"Patient", "Wellness"});
    
    JButton discharge = new JButton("Discharge");
    JButton NextPlayer = new JButton("Next Player");
    JPanel info = new JPanel(new GridLayout(2,1));
    JPanel info2 = new JPanel(new GridLayout(2,1));
    JLabel SiteName = new JLabel("Acute Hospital", JLabel.LEFT);
    JTextArea instruc = new JTextArea("The aim of the game is to minimise the number of penalty points your team receives. "
            + "If you do not have a bed for a patient you will receive a penalty of 100 points per patient without a bed. "
            + "If you discharge a patient before they are 100% better they may be readmitted. "
            + "The lower a patients wellness score at discharge the more likely they are to be readmitted. "
            + "The Acute Hospital team will receive 100 penalty points for every patient who is readmitted. "
            + "Select a patient to discharge from the list below and click the 'Discharge' button to discharge them. "
            + "You can discharge as many or as few patients as you wish or none at all. "
            + "Click the 'Next Player' button when you have finished your turn.",7,90);
    
    JTextArea instruc2 = new JTextArea("The aim of the game is to minimise the number of penalty points your team receives. "
            + "If you do not have a bed for a patient you will receive a penalty of 100 points per patient without a bed. "
            + "If you discharge a patient before they are 100% better they may be readmitted. "
            + "The lower a patients wellness score at discharge the more likely they are to be readmitted. "
            + "The Acute Hospital team will receive 100 penalty points for every patient who is readmitted. "
            + "Select a patient to discharge from the list below and click the 'Discharge' button to discharge them. "
            + "You can discharge as many or as few patients as you wish or none at all. "
            + "Click the 'Next Player' button when you have finished your turn.",7,90);
    
    JScrollPane instScroll = new JScrollPane(instruc);
    JScrollPane instScroll2 = new JScrollPane(instruc2);
    
   // JLabel ic = new JLabel(new ImageIcon("\\isad.isadroot.ex.ac.uk\\UOE\\User\\My Pictures\\PenCHORD banner.jpg"));
    
    JLabel statusLabel = new JLabel("<html>Number of patients<br> "
            + "discharged = <html>" + DisCount, JLabel.CENTER);
        
    JLabel gameText1 = new JLabel("Number of beds = " + beds, JLabel.CENTER);
    JLabel gameText2 = new JLabel("Number of patients = " + patients, JLabel.CENTER);
    JLabel NumTurns = new JLabel("Turn number " + turnCount, JLabel.CENTER);
    
    JLabel TotPenalties = new JLabel ("<html>Total penalty <br>incured ="
            + " <html>" + penaltiesInc, JLabel.CENTER);
    JLabel NumNewPat = new JLabel ("<html>Number of new <br>patients"
            + " = <html>" + newPatients, JLabel.CENTER);
    
    JPanel gameInfo = new JPanel(new GridLayout(4,2));
    
    JPanel controls = new JPanel(new GridLayout(1,2));
    
    JFrame frame2 = new JFrame("Community Hospital");
    DefaultTableModel model2 = new DefaultTableModel(new Object[][] {},
            new String[] {"Patient", "Wellness"});
    
    JButton discharge2 = new JButton("Discharge");
    JButton NextPlayer2 = new JButton("Next Player");
    JLabel SiteName2 = new JLabel("Community Hospital", JLabel.LEFT);
    
    
    JLabel statusLabel2 = new JLabel("<html>Number of patients<br> "
            + "discharged = <html>" + DisCount2, JLabel.CENTER);
        
    JLabel gameText12 = new JLabel("Number of beds = " + beds2, JLabel.CENTER);
    JLabel gameText22 = new JLabel("Number of patients = " + patients2, JLabel.CENTER);
    JLabel NumTurns2 = new JLabel("Turn number " + turnCount, JLabel.CENTER);
    
    JLabel TotPenalties2 = new JLabel ("<html>Total penalty <br>incured"
            + " = <html>" + penaltiesInc2, JLabel.CENTER);
    JLabel NumNewPat2 = new JLabel ("<html>Number of new <br>patients"
            + " = <html>" + newPatients2, JLabel.CENTER);
    
    JPanel gameInfo2 = new JPanel(new GridLayout(4,2));
    JPanel controls2 = new JPanel(new GridLayout(1,2));
    
    JFrame frame3 = new JFrame();
    DefaultTableModel model3 = new DefaultTableModel(new Object[][] {},
            new String[] {"Patient", "Wellness"});  
    
    JFrame endFrame = new JFrame();
    JPanel scores = new JPanel(new GridLayout(10,1));
    JPanel endControls = new JPanel(new GridLayout(1,1));
    JButton newGame = new JButton("New Game");
    JButton dataOut = new JButton("Output Game Data");
    JLabel gameOver = new JLabel("Game complete", JLabel.CENTER);
    JLabel acutePen = new JLabel("Penalty points incurred by the acute hospital = " + penaltiesInc, JLabel.CENTER);
    JLabel commPen = new JLabel("Penalty points incurred by the community hospital = " + penaltiesInc2, JLabel.CENTER);
    JLabel acuteName = new JLabel("Acute hospital details", JLabel.CENTER);
    JLabel acuteNumDis = new JLabel("Number of patients discharged = " + totDis, JLabel.CENTER);
    JLabel acuteNumRead = new JLabel("Number of patients readmitted = " + totRead, JLabel.CENTER);
    JLabel acuteAvPen = new JLabel("Penalty points for bed avaliability = " + totAvPen, JLabel.CENTER);
    JLabel acuteReadPen = new JLabel("Penalty points for readmissions = " + totRePen, JLabel.CENTER);
    JLabel commName = new JLabel("Community hospital details", JLabel.CENTER);
    JLabel commNumDis = new JLabel("Number of patients discharged = " + totDis2, JLabel.CENTER);
    JLabel commAvPen = new JLabel("Penalty points for bed avaliability = " + totAvPen2, JLabel.CENTER);   
    
    JPanel meanChart = new JPanel();
    
   // JOptionPane con1 = new JOptionPane("<html>Player 1 finished? "
   //                     + "<br>Click No to continue turn <br><br>If finished "
   //                     + "<br>Player 2 click yes to continue<html>", JOptionPane.YES_NO_OPTION);
    
   // JOptionPane con2 = new JOptionPane("<html>Player 2 finished? "
   //                     + "<br>Click No to continue turn <br><br>If finished "
   //                     + "<br>Player 1 click yes to continue<html>", JOptionPane.YES_NO_OPTION);

    JTable table = new JTable(model){
        public boolean isCellEditable(int row, int col){
            return false;
                }
            };
    JTable table2 = new JTable(model2){
        public boolean isCellEditable(int row, int col){
            return false;
                }
            };
    JTable table3 = new JTable(model3){
        public boolean isCellEditable(int row, int col){
            return false;
                }
            };
    
    JScrollPane scroll = new JScrollPane(table);
    JScrollPane scroll2 = new JScrollPane(table2);

    public HospitalGameV3() {
        
        initialSetup();
        
        //setSave.addActionListener(new ActionListener() {
        //    public void actionPerformed(ActionEvent arg0) {
        //        chooser = new JFileChooser();
        //        chooser.setDialogTitle(chooserTitle);
        //        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //        chooser.setAcceptAllFileFilterUsed(false);
                
        //        int n = chooser.showDialog(chooser, "Ok");
        //        if (n == JFileChooser.APPROVE_OPTION){
        //            System.out.println("Dir = " + chooser.getCurrentDirectory());
                    //System.out.println("File = " + chooser.getSelectedFile());
                    
        //            saveLocation = (chooser.getSelectedFile().getPath());
        //            System.out.println("File = " + saveLocation);
        //       } else {
        //            JOptionPane.showMessageDialog(chooser, "<html>Please ensure you provide a valid location <br>otherwise output will not be saved<html>");
        //        }
        //    }
        //});
        
        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                chooser = new JFileChooser();
                chooser.setDialogTitle(chooserTitle);
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setAcceptAllFileFilterUsed(false);
                
                int n = chooser.showDialog(chooser, "Ok");
                if (n == JFileChooser.APPROVE_OPTION){
                    System.out.println("Dir = " + chooser.getCurrentDirectory());
                    //System.out.println("File = " + chooser.getSelectedFile());
                    
                    saveLocation = (chooser.getSelectedFile().getPath());
                    System.out.println("File = " + saveLocation);
                    
                    beds = Integer.parseInt(setNumBeds.getText());
                    patients = Integer.parseInt(setNumPatients.getText());
                    wellnessInc = Integer.parseInt(setWellInc.getText());
                    admisPenalty = Integer.parseInt(setValAdPen.getText());
                    readmitPenalty = Integer.parseInt(setValReadPen.getText());
                    beds2 = Integer.parseInt(setNumBeds2.getText());
                    patients2 = Integer.parseInt(setNumPatients2.getText());
                    wellnessInc2 = Integer.parseInt(setWellInc2.getText());
                    admisPenalty2 = Integer.parseInt(setValAdPen2.getText());
                    randNewPat = Integer.parseInt(setValRandNewPat.getText());
                    randPatWell = Integer.parseInt(setValRandPatWell.getText());
                    maxTurns = Integer.parseInt(setValMaxTurn.getText());
                
                    gameText1.setText("<html>Number of <br>beds = <html>" + beds);
                    gameText2.setText("<html>Number of <br>patients = <html>" + patients);
                    gameText12.setText("<html>Number of <br>beds = <html>" + beds2);
                    gameText22.setText("<html>Number of <br>patients = <html>" + patients2);
                
                
                    CreatePatients();
                    CreatePatients2();
                    startFrame.setVisible(false);
                    frame.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(chooser, "<html>Please ensure you provide a valid location <br>otherwise output will not be saved<html>");
                }               
            }
       });
        
        startFrame.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
         int Answer = JOptionPane.showConfirmDialog(startFrame, "Are you sure you want to quit?", "Quit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
         if (Answer == JOptionPane.YES_OPTION)
             System.exit(0);
            }
        });
       
       frame.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
         int Answer = JOptionPane.showConfirmDialog(frame, "Are you sure you want to quit?", "Quit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
         if (Answer == JOptionPane.YES_OPTION)
             System.exit(0);
            }
        });
       
       frame2.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
         int Answer = JOptionPane.showConfirmDialog(frame2, "Are you sure you want to quit?", "Quit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
         if (Answer == JOptionPane.YES_OPTION)
             System.exit(0);
            }
        });
       
       endFrame.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
         int Answer = JOptionPane.showConfirmDialog(endFrame, "Are you sure you want to quit?", "Quit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
         if (Answer == JOptionPane.YES_OPTION)
             System.exit(0);
            }
        });

       table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
       
       discharge.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                DefaultTableModel model = (DefaultTableModel) table.getModel();
                //patDataOut = new ArrayList<>();
                
                //int[] temp;
                int a=table.getSelectedRow();
                int a1 = 0;
                int a2 = 1;
                int b=(int) table.getModel().getValueAt(a, a1);
                int c=(int) table.getModel().getValueAt(a, a2);
                int e = turnCount;
                meanC.add(c);
                patDataOut[counter2][0] = player;
                patDataOut[counter2][1] = e;
                patDataOut[counter2][2] = b;
                patDataOut[counter2][3] = c;

                counter2 = counter2 + 1;
                Object[] d = {b,c};
                model2.addRow(d);
                model.removeRow(a);
                
                totDis = totDis + 1;
                
                DisCount = DisCount + 1;
                patients = patients - 1;
                table.revalidate();

                statusLabel.setText("<html>Number of patients <br>discharged = <html>" + DisCount);

                gameText2.setText("<html>Number of <br>patients = <html>" + patients);
            }
        });
       
       NextPlayer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                frame.setVisible(false);
                int Answer = JOptionPane.showConfirmDialog(NextPlayer, "<html>Player 1 finished? "
                        + "<br>Click No to continue turn <br><br>If finished "
                        + "<br>Player 2 click yes to continue<html>", "Next Player", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (Answer == JOptionPane.YES_OPTION){
                    patients2 = patients2 + DisCount;
                newPatients2 = DisCount;
                int tot = 0;
                int a = meanC.size();
                if (a == 0){
                    mean = 0;
                }else {
                    for(int i=0; i<a; i++){
                        tot = tot + meanC.get(i);
                    }
                    mean = tot/a;
                }
                meanData[counter][0] = player;
                meanData[counter][1] = mean;
                meanData[counter][2] = turnCount;

                meanC.clear();                  //Array needs to be created for community hospital then JFreeChart data set and bar graph can be made
                
                counter = counter + 1;
                
                if (patients2 > beds2){
                    penaltiesInc2 = penaltiesInc2 + ((patients2 - beds2)*admisPenalty2);
                }
                totAvPen2 = penaltiesInc2;
                //System.out.println("comm pen = " + totAvPen2);
                TotPenalties2.setText("<html>Total penalty <br>incured = <html>" + penaltiesInc2);
                gameText22.setText("<html>Number of <br>patients = <html>" + patients2);
                NumNewPat2.setText("<html>Number of new <br>patients = <html>" + newPatients2);
                
                player = 2;
                    frame2.setVisible(true);
                }
                else if (Answer == JOptionPane.NO_OPTION){
                    frame.setVisible(true);
                } else if (Answer == JOptionPane.CLOSED_OPTION){
                    frame.setVisible(true);
                }        
                
            }
       });     
       
       discharge2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                DefaultTableModel model2 = (DefaultTableModel) table2.getModel();
                int a=table2.getSelectedRow();
                int a1 = 0;
                int a2 = 1;
                int b=(int) table2.getModel().getValueAt(a, a1);
                int c=(int) table2.getModel().getValueAt(a, a2);
                int e = turnCount;
                patDataOut[counter2][0] = player;
                patDataOut[counter2][1] = e;
                patDataOut[counter2][2] = b;
                patDataOut[counter2][3] = c;

                counter2 = counter2 + 1;
                
                Object[] d = {b,c};
                meanC.add(c);
                model3.addRow(d);
                model2.removeRow(a);
                
                totDis2 = totDis2 + 1;
                
                DisCount2 = DisCount2 + 1;
                patients2 = patients2 - 1;
                
                statusLabel2.setText("<html>Number of patients <br>discharged = <html>" + DisCount2);
                gameText22.setText("<html>Number of <br>patients = <html>" + patients2);
                
            }    
       });
       
       NextPlayer2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                frame2.setVisible(false);
                int Answer = JOptionPane.showConfirmDialog(NextPlayer, "<html>Player 2 finished? "
                        + "<br>Click No to continue turn <br><br>If finished "
                        + "<br>Player 1 click yes to continue<html>", "Next Player", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (Answer == JOptionPane.YES_OPTION){
                
                int tot = 0;
                int a = meanC.size();
                if (a == 0){
                    mean = 0;
                }else {
                    for(int i=0; i<a; i++){
                        tot = tot + meanC.get(i);
                    }
                    mean = tot/a;
                }
                meanData[counter][0] = player;
                meanData[counter][1] = mean;
                meanData[counter][2] = turnCount;
                
                meanC.clear(); 
                
                counter = counter + 1;
                
                turnCount = turnCount + 1;
                updateWellness();               
                CreatePatients();
                readmitPat();
                patients = patients + newAdmis + readmitTurn;
                newPatients = newAdmis + readmitTurn;
                

                if (patients > beds){
                    penaltiesInc3 = penaltiesInc3 + ((patients - beds)*admisPenalty);
                }
                
                totAvPen = totAvPen + penaltiesInc3;
                penaltiesInc = penaltiesInc + penaltiesInc3;
                if (readmitTurn > 0){
                    penaltiesInc = penaltiesInc + (readmitTurn * readmitPenalty);
                }
                penaltiesInc3 = 0;
                totRead = totRead + readmitTurn;
                totRePen = totRePen + (readmitTurn * readmitPenalty);
                //System.out.println("acute read pen = " + totRePen);
                TotPenalties.setText("<html>Total penalty <br>incured = <html>" + penaltiesInc);
                readmitTurn = 0;
                gameText2.setText("<html>Number of <br>patients = <html>" + patients);
                NumNewPat.setText("<html>Number of new <br>patients = <html>" + newPatients);
                NumTurns.setText("Turn number " + turnCount);
                NumTurns2.setText("Turn number " + turnCount);
                
                DisCount = 0;
                DisCount2 = 0;
                statusLabel.setText("<html>Number of patients <br>discharged = <html>" + DisCount);
                statusLabel2.setText("<html>Number of patients <br>discharged = <html>" + DisCount2);
                
                player = 1;
                
                if (turnCount == maxTurns + 1){
                    acutePen.setText("Penalty points incurred by the acute hospital = " + penaltiesInc);
                    commPen.setText("Penalty points incurred by the community hospital = " + penaltiesInc2);
                    acuteNumDis.setText("Number of patients discharged = " + totDis);
                    acuteNumRead.setText("Number of patients readmitted = " + totRead);
                    acuteAvPen.setText("Penalty points for bed avaliability = " + totAvPen);
                    acuteReadPen.setText("Penalty points for readmissions = " + totRePen);
                    commNumDis.setText("Number of patients discharged = " + totDis2);
                    commAvPen.setText("Penalty points for bed avaliability = " + totAvPen2);
                    summaryDataOut = new int[1][8];
                    summaryDataOut[0][0] = penaltiesInc;
                    summaryDataOut[0][1] = penaltiesInc2;
                    summaryDataOut[0][2] = totDis;
                    summaryDataOut[0][3] = totRead;
                    summaryDataOut[0][4] = totAvPen;
                    summaryDataOut[0][5] = totRePen;
                    summaryDataOut[0][6] = totDis2;
                    summaryDataOut[0][7] = totAvPen2;
                    CategoryDataset chartData = null;
                    try {
                        chartData = createDataset();
                    } catch (IOException ex) {
                        Logger.getLogger(HospitalGameV3.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(NextPlayer2, "Unable to save output");
                    }
                    final JFreeChart chart = createChart(chartData);
                    ChartPanel chartPanel = new ChartPanel(chart);
                    chartPanel.setPreferredSize(new Dimension(500,400));
                    meanChart.add(chartPanel);
                    chartPanel.getParent().validate();
                    endFrame.setVisible(true);
                    //frame.setVisible(false);
                    //
                }else {
                    //frame2.setVisible(false);
                    frame.setVisible(true);
                }
                }
                else if (Answer == JOptionPane.NO_OPTION){
                    frame2.setVisible(true);
                }
                else if (Answer == JOptionPane.CLOSED_OPTION){
                    frame2.setVisible(true);
                }
            }
       });
       
       newGame.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                turnCount = 1;
                penaltiesInc = 0;
                newPatients = 0;
                newAdmis = 0;
                DisNum = 0;
                DisCount = 0;

                penaltiesInc2 = 0;
                newPatients2 = 0;
                DisCount2 = 0;

                numReadmit = 0;
                readmitTurn = 0;
                numPatCreated = 0;
                
                totDis = 0;
                totDis2 = 0;
                totRead = 0;
                totAvPen = 0;
                totAvPen2 = 0;
                totRePen = 0;
                
                NumTurns.setText("Turn number " + turnCount);
                NumTurns2.setText("Turn number " + turnCount);
                TotPenalties.setText("<html>Total penalty <br>incured = <html>" + penaltiesInc);
                TotPenalties2.setText("<html>Total penalty <br>incured = <html>" + penaltiesInc2);
                
                meanCalc = null;
                meanCalc2 = null;
                meanData = null;
                meanData2 = null;
                mean = 0;
                player = 1;
                counter = 0;
                counter2 = 0;
                penaltiesInc3 = 0;
                meanC = null;
                patDataOut = null;
                summaryDataOut = null;
                
                DefaultTableModel model = (DefaultTableModel) table.getModel();
                model.setRowCount(0);
                
                DefaultTableModel model2 = (DefaultTableModel) table2.getModel();
                model2.setRowCount(0);
                
                DefaultTableModel model3 = (DefaultTableModel) table3.getModel();
                model3.setRowCount(0);
                
                endFrame.setVisible(false);
                startFrame.setVisible(true);
            }    
       });    

       setLayout();

        startFrame.add(gameName, BorderLayout.NORTH);
        acuteSettings.setOpaque(false);
        startFrame.add(acuteSettings);
        startControls.add(start);
        startControls.setOpaque(false);
        //startControls.add(setSave);
        startFrame.add(startControls, BorderLayout.SOUTH);
        startFrame.setPreferredSize(new Dimension(750, 500));
        startFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        startFrame.pack();
        startFrame.setLocationRelativeTo(null);
        startFrame.setVisible(true);
        
        instScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        instScroll.setPreferredSize(new Dimension(800,100));
        info.add(SiteName);
        info.add(instScroll);
        info.setOpaque(false);
        gameInfo.add(gameText2);
        gameInfo.add(gameText1);
        gameInfo.add(NumNewPat);
        gameInfo.add(statusLabel);
        gameInfo.add(TotPenalties);
        gameInfo.add(NumTurns);
        gameInfo.setOpaque(false);
        gameInfo.setPreferredSize(new Dimension(400,500));
        controls.add(discharge);
        controls.add(NextPlayer);
        controls.setOpaque(false);
        frame.add(info, BorderLayout.NORTH);
        frame.add(gameInfo, BorderLayout.EAST);
        //frame.add(new JScrollPane(table), BorderLayout.CENTER);
        scroll.setPreferredSize(new Dimension(300,500));
        frame.add(scroll, BorderLayout.CENTER);
        frame.add(controls, BorderLayout.SOUTH);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setPreferredSize(new Dimension(800,600));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(false);
        
        instScroll2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        instScroll2.setPreferredSize(new Dimension(800,100));
        info2.add(SiteName2);
        info2.add(instScroll2);
        info2.setOpaque(false);
        gameInfo2.add(gameText22);
        gameInfo2.add(gameText12);
        gameInfo2.add(NumNewPat2);
        gameInfo2.add(statusLabel2);
        gameInfo2.add(TotPenalties2);
        gameInfo2.add(NumTurns2);
        gameInfo2.setOpaque(false);
        gameInfo2.setPreferredSize(new Dimension(400,500));
        controls2.add(discharge2);
        controls2.add(NextPlayer2);
        controls2.setOpaque(false);
        frame2.add(info2, BorderLayout.NORTH);
        frame2.add(gameInfo2, BorderLayout.EAST);
        //frame2.add(new JScrollPane(table2), BorderLayout.CENTER);
        scroll2.setPreferredSize(new Dimension(300,500));
        frame2.add(scroll2, BorderLayout.CENTER);
        frame2.add(controls2, BorderLayout.SOUTH);
        frame2.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame2.setPreferredSize(new Dimension(800,600));
        frame2.pack();
        frame2.setLocationRelativeTo(null);
        frame2.setVisible(false);
        
        frame3.add(new JScrollPane(table3), BorderLayout.CENTER);
        frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame3.pack();
        frame3.setLocationRelativeTo(null);
        frame3.setVisible(false);
        
        scores.add(acutePen);
        scores.add(commPen);
        scores.add(acuteName);
        scores.add(acuteNumDis);
        scores.add(acuteNumRead);
        scores.add(acuteAvPen);
        scores.add(acuteReadPen);
        scores.add(commName);
        scores.add(commNumDis);
        scores.add(commAvPen);
        scores.setOpaque(false);
        //chartPanel.setPreferredSize(new Dimension(400, 325));
        //meanChart.add(chartPanel);
        endControls.add(newGame);
        endControls.setOpaque(false);
       // endControls.add(dataOut);
        meanChart.setOpaque(false);
        endFrame.add(meanChart, BorderLayout.WEST);
        endFrame.add(endControls, BorderLayout.SOUTH);
        endFrame.add(gameOver, BorderLayout.NORTH);
        endFrame.add(scores, BorderLayout.EAST);
        endFrame.setPreferredSize(new Dimension(1000, 750));
        endFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        endFrame.pack();
        endFrame.setLocationRelativeTo(null);
        endFrame.setVisible(false);
        
        
    }
    
    public void initialSetup(){
        buttonFont = new Font("Arial", Font.PLAIN, 16);
        start.setFont(buttonFont);
        setSave.setFont(buttonFont);
        discharge.setFont(buttonFont);
        NextPlayer.setFont(buttonFont);
        discharge2.setFont(buttonFont);
        NextPlayer2.setFont(buttonFont);
        newGame.setFont(buttonFont);
        instruc.setFont(new Font("Arial", Font.PLAIN, 14));
        instruc.setLineWrap(true);
        instruc.setWrapStyleWord(true);
        instruc2.setFont(new Font("Arial", Font.PLAIN, 14));
        instruc2.setLineWrap(true);
        instruc2.setWrapStyleWord(true);
        TitledBorder instTitle = BorderFactory.createTitledBorder("Instructions");
        instScroll.setBorder(instTitle);
        instScroll2.setBorder(instTitle);
        info.setBorder(BorderFactory.createEmptyBorder(0,15,15,15));
        info2.setBorder(BorderFactory.createEmptyBorder(0,15,15,15));
        scoreFont = new Font("Serif", Font.PLAIN, 14);
        statusLabel.setFont(scoreFont);
        statusLabel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        gameText1.setFont(scoreFont);
        gameText1.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        gameText2.setFont(scoreFont);
        gameText2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        NumTurns.setFont(scoreFont);
        NumTurns.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        TotPenalties.setFont(scoreFont);
        TotPenalties.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        NumNewPat.setFont(scoreFont);
        NumNewPat.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        statusLabel2.setFont(scoreFont);
        statusLabel2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        gameText12.setFont(scoreFont);
        gameText12.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        gameText22.setFont(scoreFont);
        gameText22.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        NumTurns2.setFont(scoreFont);
        NumTurns2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        TotPenalties2.setFont(scoreFont);
        TotPenalties2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        NumNewPat2.setFont(scoreFont);
        NumNewPat2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        gameName.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        startFrame.getContentPane().setBackground(back);
        frame.getContentPane().setBackground(back);
        frame2.getContentPane().setBackground(back);
        endFrame.getContentPane().setBackground(back);
        patDataOut = new int[maxTurns*patients2][4];
        readDataOut = new int[maxTurns*patients2][3];
        meanC = new ArrayList<>();
        acuteSetName.setFont(new Font("Serif",Font.BOLD, 18));
        commSetName.setFont(new Font("Serif",Font.BOLD, 18));
        otherSetName.setFont(new Font("Serif",Font.BOLD, 18));
        setNumBeds.setHorizontalAlignment(JTextField.CENTER);
        setNumPatients.setHorizontalAlignment(JTextField.CENTER);
        setWellInc.setHorizontalAlignment(JTextField.CENTER);
        setValAdPen.setHorizontalAlignment(JTextField.CENTER);
        setValReadPen.setHorizontalAlignment(JTextField.CENTER);
        setNumBeds2.setHorizontalAlignment(JTextField.CENTER);
        setNumPatients2.setHorizontalAlignment(JTextField.CENTER);
        setWellInc2.setHorizontalAlignment(JTextField.CENTER);
        setValAdPen2.setHorizontalAlignment(JTextField.CENTER);
        setValRandNewPat.setHorizontalAlignment(JTextField.CENTER);
        setValRandPatWell.setHorizontalAlignment(JTextField.CENTER);
        setValMaxTurn.setHorizontalAlignment(JTextField.CENTER);
        gameName.setFont(new Font("Serif",Font.BOLD, 45));
        gameOver.setFont(new Font("Serif",Font.BOLD, 35));
        SiteName.setFont(new Font("Serif",Font.BOLD, 35));
        SiteName.setBorder(BorderFactory.createEmptyBorder(5,15,5,15));
        SiteName2.setBorder(BorderFactory.createEmptyBorder(5,15,5,15));
        SiteName2.setFont(new Font("Serif",Font.BOLD, 35));
        gameInfo.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        //gameInfo.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        gameInfo2.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        table.setFont(new Font("Arial", Font.PLAIN, 13));
        table.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 14));
        table2.setFont(new Font("Arial", Font.PLAIN, 13));
        table2.getTableHeader().setFont(new Font("Arial", Font.PLAIN, 14));
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        ((DefaultTableCellRenderer)table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        ((DefaultTableCellRenderer)table2.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        
        table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        table2.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        table2.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        acuteName.setFont(new Font("Arial",Font.BOLD, 18));
        commName.setFont(new Font("Arial",Font.BOLD, 18));
        acutePen.setFont(new Font("Arial",Font.BOLD, 16));
        commPen.setFont(new Font("Arial",Font.BOLD, 16));
        scores.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
    }
    
    public void setLayout(){
        
        GridBagConstraints c = new GridBagConstraints();

    c.gridx = 0;
    c.gridy = 0;
    c.ipadx = 60;
    c.ipady = 10;
    acuteSettings.add(acuteSetName, c);
    
    c.gridx = 0;
    c.gridy = 1;
    acuteSettings.add(setBeds, c);
    
    c.gridx = 1;
    c.gridy = 1;
    acuteSettings.add(setNumBeds, c);

    c.gridx = 0;
    c.gridy = 2;
    acuteSettings.add(setPatients, c);

    c.gridx = 1;
    c.gridy = 2;
    acuteSettings.add(setNumPatients, c);

    c.gridx = 0;
    c.gridy = 3;
    acuteSettings.add(setWell, c);

    c.gridx = 1;
    c.gridy = 3;
    acuteSettings.add(setWellInc, c);

    c.gridx = 0;
    c.gridy = 4;
    acuteSettings.add(setAdPen, c);

    c.gridx = 1;
    c.gridy = 4;
    acuteSettings.add(setValAdPen, c);

    c.gridx = 0;
    c.gridy = 5;
    acuteSettings.add(setReadPen, c);

    c.gridx = 1;
    c.gridy = 5;
    acuteSettings.add(setValReadPen, c);

    c.gridx = 2;
    c.gridy = 0;
    acuteSettings.add(commSetName, c);
    
    c.gridx = 2;
    c.gridy = 1;
    acuteSettings.add(setBeds2, c);

    c.gridx = 3;
    c.gridy = 1;
    acuteSettings.add(setNumBeds2, c);

    c.gridx = 2;
    c.gridy = 2;
    acuteSettings.add(setPatients2, c);

    c.gridx = 3;
    c.gridy = 2;
    acuteSettings.add(setNumPatients2, c);

    c.gridx = 2;
    c.gridy = 3;
    acuteSettings.add(setWell2, c);

    c.gridx = 3;
    c.gridy = 3;
    acuteSettings.add(setWellInc2, c);

    c.gridx = 2;
    c.gridy = 4;
    acuteSettings.add(setAdPen2, c);

    c.gridx = 3;
    c.gridy = 4;
    acuteSettings.add(setValAdPen2, c);
    
    c.gridx = 0;
    c.gridy = 7;
    acuteSettings.add(otherSetName, c);
    
    c.gridx = 0;
    c.gridy = 9;
    acuteSettings.add(setRandNewPat, c);
    
    c.gridx = 1;
    c.gridy = 9;
    acuteSettings.add(setValRandNewPat, c);
    
    c.gridx = 2;
    c.gridy = 9;
    acuteSettings.add(setRandPatWell, c);
    
    c.gridx = 3;
    c.gridy = 9;
    acuteSettings.add(setValRandPatWell, c);
    
    c.gridx = 2;
    c.gridy = 8;
    acuteSettings.add(setMaxTurn, c);
    
    c.gridx = 3;
    c.gridy = 8;
    acuteSettings.add(setValMaxTurn, c);
        
    }

    public void CreatePatients(){
        if(turnCount == 1){
            int n = patients;
            patNum = new Object[n][2];
            Random randomWell = new Random();
            for (int i=0; i<n; i++) {
                patNum[i][0] = i + 1;
                patNum[i][1] = randomWell.nextInt(randPatWell) + 1;
                model.addRow(patNum[i]);
            }
        numPatCreated = numPatCreated + n;
        } else if (turnCount > 1){
            Random randomGenerator = new Random();
            newAdmis = randomGenerator.nextInt(randNewPat);
            Random randomWell = new Random();
            for (int i=0; i<newAdmis; i++) {
                patNum[i][0] = i + numPatCreated + 1;
                patNum[i][1] = randomWell.nextInt(randPatWell) + 1;
                model.addRow(patNum[i]);
            }
        numPatCreated = numPatCreated + newAdmis;
        }
    }
    
    public void CreatePatients2(){
        if(turnCount == 1){
            int n = patients2;
            patNum2 = new Object[n][2];
            Random randomWell = new Random();
            for (int i=0; i<n; i++) {
                patNum2[i][0] = i + numPatCreated + 1;
                patNum2[i][1] = randomWell.nextInt(80) + 1;
                model2.addRow(patNum2[i]);
            }
        numPatCreated = numPatCreated + n;
        }
    }
    
    public void updateWellness(){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
            int n = table.getRowCount();
            Object[][] d = new Object[n][2];
            for (int i=0; i<n; i++){
                d[i][0]=(int) table.getModel().getValueAt(i, 0);
                int b=(int) table.getModel().getValueAt(i, 1);
                if (b < 100){
                    b = b + wellnessInc;
                } else if (b >= 100){
                    // do nothing
                }
                if (b > 100){
                    b = 100;
                }
                d[i][1] = b;
                model.addRow(d[i]);
                
            }
            
            for(int i=n-1; i>=0; i--){
                model.removeRow(i);
            }
            
        DefaultTableModel model2 = (DefaultTableModel) table2.getModel();    
            int m = table2.getRowCount();
            Object[][] e = new Object[m][2];
            for (int i=0; i<m; i++){
                e[i][0]=(int) table2.getModel().getValueAt(i, 0);
                int b=(int) table2.getModel().getValueAt(i, 1);
                if (b < 100){
                    b = b + wellnessInc2;
                } else if (b >= 100){
                    // do nothing
                }
                if (b > 100){
                    b = 100;
                }
                e[i][1] = b;
                model2.addRow(e[i]);
                
            }
            for(int i=m-1; i>=0; i--){
                model2.removeRow(i);
            }
    }
    private CategoryDataset createDataset() throws FileNotFoundException, IOException{
        final DefaultCategoryDataset chartData = new DefaultCategoryDataset();
        
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
        String file = saveLocation + "\\means" + dateFormat.format(now) + ".csv";
        System.out.println(file);
        exportDouble(file, meanData);
        
        String file2 = saveLocation + "\\discharges" + dateFormat.format(now) + ".csv";
        exportInteger(file2, patDataOut);
                
        String file3 = saveLocation + "\\summary" + dateFormat.format(now) + ".csv";
        exportInteger2(file3, summaryDataOut);
        
        String file4 = saveLocation + "\\readmissions" + dateFormat.format(now) + ".csv";
        exportInteger3(file4, readDataOut);
        
        int c = (maxTurns*2);
        for (int i=0; i<c; i++){
            double a = meanData[i][0];
            double b = meanData[i][1];
            double d = meanData[i][2];
            //System.out.println("row = " + i);
            //System.out.println("player = " + a);
            //System.out.println("mean = " + b);
            String turn = String.valueOf(d);
            if (a == 1){
                //String turn = String.valueOf(i+1);
                String hos = "Acute";
                chartData.addValue(b,hos,turn);
            } else if (a == 2){
                //String turn = String.valueOf(i);
                String hos = "Community";
                chartData.addValue(b,hos,turn);
            }
        }
        return chartData;
    }
    
    private JFreeChart createChart(final CategoryDataset chartData){
        final JFreeChart barChart = ChartFactory.createBarChart("Mean Chart",
                                "Turn", "Mean Wellness at Discharge", chartData, 
                                PlotOrientation.VERTICAL, true, true, false);
        
        return barChart;
    }
    
    public void readmitPat(){
        DefaultTableModel model3 = (DefaultTableModel) table3.getModel();

        int a = table3.getRowCount();
        if (a > 0){
            Object[][] d = new Object[a][2];
            //Object[] g = new Object[a];
            for (int i=a-1; i>=0; i--){
                double b = (int) table3.getModel().getValueAt(i, 1);
                if (b < 100){
                    //System.out.println(b);
                    double c = (100 - b)/100;
                    //System.out.println(c);
                    Random e = new Random();
                    double f = e.nextDouble();
                    //System.out.println(f + " <= " + c );
                    if (f <= c){
                        d[i][0] = (int) table3.getModel().getValueAt(i, 0);
                        int h = (int) table3.getModel().getValueAt(i, 1);
                        d[i][1] = h - (h/4);
                        model.addRow(d[i]);
                        readDataOut[numReadmit][0] = turnCount;
                        readDataOut[numReadmit][1] = (int) table3.getModel().getValueAt(i, 0);
                        readDataOut[numReadmit][2] = h;
                        model3.removeRow(i);
                        numReadmit = numReadmit + 1;
                        readmitTurn = readmitTurn + 1;
                        //System.out.println("Patient readmitted" + d[1][0]);
                    }
                }
            }
        }
    }
    
    public static void exportDouble(String fileName, double[][] meanData)throws FileNotFoundException, IOException{
        File file = new File(fileName);
        if (!file.isFile())
            file.createNewFile();
        
        CSVWriter csvWriter = new CSVWriter(new FileWriter(file));
        String[] header = {"Player", "Mean", "Turn"};
        csvWriter.writeNext(header);        
        int rowCount = meanData.length;
        
        for (int i=0; i<rowCount; i++){
            int columnCount = meanData[i].length;
            String[] values = new String[columnCount];
            for (int j=0; j<columnCount; j++){
                values[j] = meanData[i][j] + "";
            }
            csvWriter.writeNext(values);
        }
        csvWriter.flush();
        csvWriter.close();
    }
    
    public static void exportInteger(String fileName, int[][] patDataOut)throws FileNotFoundException, IOException{
        File file = new File(fileName);
        if (!file.isFile())
            file.createNewFile();
        
        CSVWriter csvWriter = new CSVWriter(new FileWriter(file));
        String[] header = {"Player", "Turn", "Patient", "Wellness"};
        csvWriter.writeNext(header);
        //System.out.println(patDataOut[0][0]);
        
        int rowCount = patDataOut.length;
        //System.out.println(rowCount);
        for (int i=0; i<rowCount; i++){
            int columnCount = patDataOut[i].length;
            String[] values = new String[columnCount];
            for (int j=0; j<columnCount; j++){
                values[j] = patDataOut[i][j] + "";
            }
            csvWriter.writeNext(values);
        }
        csvWriter.flush();
        csvWriter.close();
    }
    
    public static void exportInteger2(String fileName, int[][] summaryDataOut)throws FileNotFoundException, IOException{
        File file = new File(fileName);
        if (!file.isFile())
            file.createNewFile();
        
        CSVWriter csvWriter = new CSVWriter(new FileWriter(file));
        
        String[] header = {"AcutePen", "CommPen", "AcuteDis", "AcuteRead", "AcuteAvPen",
            "AcuteReadPen", "CommDis", "CommAvPen"};
        csvWriter.writeNext(header);
        
        int rowCount = summaryDataOut.length;
        
        for (int i=0; i<rowCount; i++){
            int columnCount = summaryDataOut[i].length;
            String[] values = new String[columnCount];
            for (int j=0; j<columnCount; j++){
                values[j] = summaryDataOut[i][j] + "";
            }
            csvWriter.writeNext(values);
        }
        csvWriter.flush();
        csvWriter.close();
    }
    
    public static void exportInteger3(String fileName, int[][] readDataOut)throws FileNotFoundException, IOException{
        File file = new File(fileName);
        if (!file.isFile())
            file.createNewFile();
        
        CSVWriter csvWriter = new CSVWriter(new FileWriter(file));
        String[] header = {"Turn", "Patient", "Wellness"};
        csvWriter.writeNext(header);
        //System.out.println(patDataOut[0][0]);
        
        int rowCount = readDataOut.length;
        //System.out.println(rowCount);
        for (int i=0; i<rowCount; i++){
            int columnCount = readDataOut[i].length;
            String[] values = new String[columnCount];
            for (int j=0; j<columnCount; j++){
                values[j] = readDataOut[i][j] + "";
            }
            csvWriter.writeNext(values);
        }
        csvWriter.flush();
        csvWriter.close();
    }
    
    public static void main(String[] args) {
        
        try {
            // Set cross-platform Java L&F (also called "Metal")
        UIManager.setLookAndFeel(
            UIManager.getSystemLookAndFeelClassName());
            } 
            catch (UnsupportedLookAndFeelException e) {
       // handle exception
            }
            catch (ClassNotFoundException e) {
       // handle exception
            }
            catch (InstantiationException e) {
       // handle exception
            }
            catch (IllegalAccessException e) {
       // handle exception
        }
        
        SwingUtilities.invokeLater(new Runnable(){
            public void run() {
                new HospitalGameV3();
            }
        });
    }
    
}
